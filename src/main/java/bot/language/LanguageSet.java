package bot.language;

/**
 * Абстрактный класс для набора языковых констант
 *
 * @author Chelnokov Egor
 */
public abstract class LanguageSet {
    String GREETING;
    String WRITE_TRACK_TITLE;
    String WRITE_ARTIST_NAME;
    String INSERT_SPOTIFY_LINK;
    String NEXT;
    String CHOOSE_TRACK;
    String LOADING_MESSAGE;
    String CHOOSE_LANG;
    String CANT_DOWNLOAD;
    String SPOTIFY_CANT_ACCESS;
    String NO_TRACK_FOUND;
    String CHANGE_SUCCESS;

    public LanguageSet(String GREETING, String WRITE_TRACK_TITLE,
                       String WRITE_ARTIST_NAME, String INSERT_SPOTIFY_LINK,
                       String NEXT, String CHOOSE_TRACK, String LOADING_MESSAGE,
                       String CHOOSE_LANG, String CHANGE_SUCCESS,
                       String CANT_DOWNLOAD, String SPOTIFY_CANT_ACCESS,
                       String NO_TRACK_FOUND) {
        this.GREETING = GREETING;
        this.WRITE_TRACK_TITLE = WRITE_TRACK_TITLE;
        this.WRITE_ARTIST_NAME = WRITE_ARTIST_NAME;
        this.INSERT_SPOTIFY_LINK = INSERT_SPOTIFY_LINK;
        this.NEXT = NEXT;
        this.CHOOSE_TRACK = CHOOSE_TRACK;
        this.LOADING_MESSAGE = LOADING_MESSAGE;
        this.CHOOSE_LANG = CHOOSE_LANG;
        this.CHANGE_SUCCESS = CHANGE_SUCCESS;
        this.CANT_DOWNLOAD = CANT_DOWNLOAD;
        this.SPOTIFY_CANT_ACCESS = SPOTIFY_CANT_ACCESS;
        this.NO_TRACK_FOUND = NO_TRACK_FOUND;
    }

    public String getCANT_DOWNLOAD() {
        return CANT_DOWNLOAD;
    }

    public String getGREETING() {
        return GREETING;
    }

    public String getWRITE_TRACK_TITLE() {
        return WRITE_TRACK_TITLE;
    }

    public String getWRITE_ARTIST_NAME() {
        return WRITE_ARTIST_NAME;
    }

    public String getINSERT_SPOTIFY_LINK() {
        return INSERT_SPOTIFY_LINK;
    }

    public String getNEXT() {
        return NEXT;
    }

    public String getCHOOSE_TRACK() {
        return CHOOSE_TRACK;
    }

    public String getLOADING_MESSAGE() {
        return LOADING_MESSAGE;
    }

    public String getCHOOSE_LANG() {
        return CHOOSE_LANG;
    }

    public String getSPOTIFY_CANT_ACCESS() {
        return SPOTIFY_CANT_ACCESS;
    }

    public String getNO_TRACK_FOUND() {
        return NO_TRACK_FOUND;
    }

    public String getCHANGE_SUCCESS() {
        return CHANGE_SUCCESS;
    }
}
