package bot.language;

import bot.language.sets.ChineseLangSet;
import bot.language.sets.EnglishLangSet;
import bot.language.sets.RussianLangSet;

/**
 * Перечисление доступных наборов языковых констант
 *
 * @author Chelnokov Egor
 * @see LanguageSet
 */
public enum Language {
    RU("Русский", "ru", new RussianLangSet()),
    ENG("English", "en", new EnglishLangSet()),
    CH("中国语文科", "ch", new ChineseLangSet());

    final String name;
    final String code;
    final LanguageSet languageSet;

    Language(String name, String code, LanguageSet language) {
        this.name = name;
        this.code = code;
        this.languageSet = language;
    }

    /**
     * возвращает объект Language по его коду
     *
     * @param code код языка
     * @return найденный объект Language или русский по умолчанию
     */
    public static Language getByCode(String code) {
        for (Language value : values()) {
            if (value.getCode().equals(code)) {
                return value;
            }
        }
        return RU;
    }

    public String getName() {
        return name;
    }

    public LanguageSet getLanguageSet() {
        return languageSet;
    }

    public String getCode() {
        return code;
    }
}
