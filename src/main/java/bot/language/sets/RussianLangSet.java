package bot.language.sets;

import bot.language.LanguageSet;

/**
 * Класс, содержащий строковые константы для русского языка
 *
 * @author Chelnokov Egor
 */
public class RussianLangSet extends LanguageSet {
    public static final String GREETING = "Привет, с помощью этого бота вы сможете найти и скачать музыку с Spotify. " +
            "Отправьте ссылку на трек, альбом, плейлист, артиста или название трека и получите свою музыку!\n\n" +
            "/lang - Язык бота | Language settings | 语言设置";
    public static final String WRITE_TRACK_TITLE = "Напишите название трека";
    public static final String WRITE_ARTIST_NAME = "Напишите имя артиста";
    public static final String INSERT_SPOTIFY_LINK = "Вставьте ссылку из Spotify";
    public static final String NEXT = "Далее";
    public static final String CHOOSE_TRACK = "Выберите трек";
    public static final String LOADING_MESSAGE = "Загрузка. Пожалуйста, подождите...";
    public static final String CHOOSE_LANG = "Выберите язык";
    public static final String CHANGE_SUCCESS = "Язык был изменён";
    public static final String CANT_DOWNLOAD = "Не удалось скачать трек. Попробуйте позже";
    public static final String SPOTIFY_CANT_ACCESS = "Не удаётся подключиться к Spotify. Попробуйте позже";
    public static final String NO_TRACK_FOUND = "Музыка по данному запросу не была найдена";

    public RussianLangSet() {
        super(GREETING, WRITE_TRACK_TITLE,
                WRITE_ARTIST_NAME, INSERT_SPOTIFY_LINK,
                NEXT, CHOOSE_TRACK, LOADING_MESSAGE,
                CHOOSE_LANG, CHANGE_SUCCESS, CANT_DOWNLOAD,
                SPOTIFY_CANT_ACCESS, NO_TRACK_FOUND);

    }
}
