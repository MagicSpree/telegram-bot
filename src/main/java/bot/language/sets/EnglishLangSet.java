package bot.language.sets;

import bot.language.LanguageSet;

/**
 * Класс, содержащий строковые константы для английского языка
 *
 * @author Chelnokov Egor
 */
public class EnglishLangSet extends LanguageSet {
    public static final String GREETING = "Hello! You may find and download music from Spotify using this bot." +
            "Use the commands, send titles and links and get tracks!\n\n" +
            "/lang - Language settings | Язык бота | 语言设置";
    public static final String WRITE_TRACK_TITLE = "Text the title of the track";
    public static final String WRITE_ARTIST_NAME = "Text the artist's name";
    public static final String INSERT_SPOTIFY_LINK = "Insert Spotify link";
    public static final String NEXT = "Next";
    public static final String CHOOSE_TRACK = "Choose the track";
    public static final String LOADING_MESSAGE = "Loading, please wait...";
    public static final String CHOOSE_LANG = "Choose the language";
    public static final String CHANGE_SUCCESS = "Language has been changed";
    public static final String CANT_DOWNLOAD = "Failed to download the track. Try again later";
    public static final String SPOTIFY_CANT_ACCESS = "Unable to connect to Spotify. Try again later";
    public static final String NO_TRACK_FOUND = "Sorry, no music was found";

    public EnglishLangSet() {
        super(GREETING, WRITE_TRACK_TITLE, WRITE_ARTIST_NAME,
                INSERT_SPOTIFY_LINK, NEXT, CHOOSE_TRACK,
                LOADING_MESSAGE, CHOOSE_LANG, CHANGE_SUCCESS,
                CANT_DOWNLOAD, SPOTIFY_CANT_ACCESS, NO_TRACK_FOUND);
    }
}
