package bot.language.sets;

import bot.language.LanguageSet;

/**
 * Класс, содержащий строковые константы для китайского языка
 *
 * @author Chelnokov Egor
 */
public class ChineseLangSet extends LanguageSet {
    public static final String GREETING = "你好！ 您可以使用此机器人从Spotify找到并下载音乐。\n" +
            "使用命令，发送标题和链接，并获得曲目！\n" +
            "\n" +
            "/lang - 语言设置 | Language settings | Язык бота";
    public static final String WRITE_TRACK_TITLE = "曲目名称 ?";
    public static final String WRITE_ARTIST_NAME = "艺术家姓名 ?";
    public static final String INSERT_SPOTIFY_LINK = "从Spotify插入链接";
    public static final String NEXT = "下一个";
    public static final String CHOOSE_TRACK = "选择轨道";
    public static final String LOADING_MESSAGE = "请等待装货. . .";
    public static final String CHOOSE_LANG = "选择语言";
    public static final String CHANGE_SUCCESS = "语言已更改";
    public static final String CANT_DOWNLOAD = "曲目无法下载。 稍后再试";
    public static final String SPOTIFY_CANT_ACCESS = "无法连接到Spotify。   稍后再试";
    public static final String NO_TRACK_FOUND = "没有找到音乐";

    public ChineseLangSet() {
        super(GREETING, WRITE_TRACK_TITLE,
                WRITE_ARTIST_NAME, INSERT_SPOTIFY_LINK,
                NEXT, CHOOSE_TRACK, LOADING_MESSAGE,
                CHOOSE_LANG, CHANGE_SUCCESS, CANT_DOWNLOAD,
                SPOTIFY_CANT_ACCESS, NO_TRACK_FOUND);
    }
}
