package bot;

import bot.language.Language;
import bot.language.LanguageSet;
import bot.language.sets.RussianLangSet;
import org.apache.log4j.Logger;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendAudio;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import se.michaelthelin.spotify.model_objects.specification.Track;
import spotify.SearchTracksController;
import spotify.SpotifyAuthorizator;
import spotify.exception.SpotifyAPIException;
import spotify.searchers.TrackSearcher;
import musicDownload.exception.NoFindMusicException;
import musicDownload.getter.MusicGetter;
import musicDownload.getter.YoutubeGetter;
import musicDownload.getter.ZaycevGetter;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static bot.language.Language.values;

public class MusicBot extends TelegramLongPollingBot {

    protected static final Logger log = Logger.getLogger(MusicBot.class);
    List<String> commandList = new ArrayList<>();
    LanguageSet languageSet = new RussianLangSet();

    @Override
    public void onUpdateReceived(Update update) {
        try {
            if (update.hasMessage()) {
                handleCommand(update);
            } else if (update.hasCallbackQuery()) {
                handleCallback(update);
            }
        } catch (SpotifyAPIException e) {
            sendAnswer(update, languageSet.getSPOTIFY_CANT_ACCESS());
            log.error(e);
        } catch (Exception e) {
            sendAnswer(update, languageSet.getCANT_DOWNLOAD());
            log.error(e);
        }
    }

    private void handleCommand(Update update) throws TelegramApiException, SpotifyAPIException, IOException {
        String text = update.getMessage().getText();
        commandList.add(text);
        switch (Command.fromString(text)) {
            case START:
                sendAnswer(update, languageSet.getGREETING());
                commandList.clear();
                break;
            case LANG:
                handleLanguageSettings(update);
                commandList.clear();
                break;
            case TRACK:
                sendAnswer(update, languageSet.getWRITE_TRACK_TITLE());
                break;
            case ARTIST:
                sendAnswer(update, languageSet.getWRITE_ARTIST_NAME());
                break;
            case LINK:
                sendAnswer(update, languageSet.getINSERT_SPOTIFY_LINK());
                break;
            default:
                if (Command.fromString(commandList.get(0)) == Command.NONE) {
                    commandList.clear();
                    break;
                }
                handleMessage(update, Command.fromString(commandList.get(0)));
                commandList.clear();
                break;
        }
    }

    private void handleLanguageSettings(Update update) throws TelegramApiException {
        List<List<InlineKeyboardButton>> allButtons =
                Arrays.stream(values())
                        .map(language -> List.of(getLangButton(language)))
                        .collect(Collectors.toList());
        InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
        markupInline.setKeyboard(allButtons);
        SendMessage sendMessage = SendMessage.builder()
                .replyMarkup(markupInline)
                .text(languageSet.getCHOOSE_LANG())
                .chatId(getUpdateChatId(update))
                .build();
        execute(sendMessage);
    }

    private InlineKeyboardButton getLangButton(Language lang) {
        return getButton(lang.getName(), "lang:" + lang.getCode());
    }

    private InlineKeyboardButton getButton(String text, String data) {
        return InlineKeyboardButton.builder()
                .text(text)
                .callbackData(data)
                .build();
    }

    private void sendAnswer(Update update, String message) {
        try {
            execute(SendMessage.builder()
                    .text(message)
                    .chatId(getUpdateChatId(update))
                    .build());
        } catch (TelegramApiException e) {
            log.error(e);
        }
    }

    private String getUpdateChatId(Update update) {
        if (update.hasMessage()) {
            return update.getMessage().getChatId().toString();
        } else {
            return update.getCallbackQuery().getMessage().getChatId().toString();
        }
    }

    private void handleMessage(Update update, Command commands)
            throws TelegramApiException, SpotifyAPIException, IOException {
        Message message = update.getMessage();
        sendAnswer(update, languageSet.getLOADING_MESSAGE());
        int offset = 0;
        String query = commands.value + " " + message.getText();
        printTrackMessage(update, offset, query);
    }

    private void printTrackMessage(Update update, int offset, String query)
            throws TelegramApiException, SpotifyAPIException, IOException {

        List<List<InlineKeyboardButton>> allButtons = getTrackButtons(query, offset);

        if (allButtons.size() == 0) {
            sendAnswer(update, languageSet.getNO_TRACK_FOUND());
        } else {
            if (allButtons.size() == 10) {
                offset += 10;
                InlineKeyboardButton button = getButton(languageSet.getNEXT(), query + ":" + offset);
                allButtons.add(List.of(button));
            }
            InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
            markupInline.setKeyboard(allButtons);

            execute(SendMessage.builder()
                    .replyMarkup(markupInline)
                    .text(languageSet.getCHOOSE_TRACK())
                    .chatId(getUpdateChatId(update))
                    .build());
        }
    }

    private List<List<InlineKeyboardButton>> getTrackButtons(String string, int offset) throws
            SpotifyAPIException, IOException {

        SpotifyAuthorizator.init();
        Track[] foundTracks = SearchTracksController.search(string, offset);

        return Arrays.stream(foundTracks)
                .map(n -> {
                    InlineKeyboardButton button = getButton(TrackSearcher.getTrackTitle(n.getId()), "id:" + n.getId());
                    return List.of(button);
                })
                .collect(Collectors.toList());
    }

    private void handleCallback(Update update)
            throws TelegramApiException, NoFindMusicException, SpotifyAPIException,
            IOException {
        String[] data = update.getCallbackQuery().getData().split(":");
        switch (data[0]) {
            case "lang":
                setLanguage(update, Language.getByCode(data[1]).getLanguageSet());
                break;
            case "id":
                String title = TrackSearcher.getTrackTitle(data[1]);
                sendMusic(update, title);
                break;
            default:
                printTrackMessage(update, Integer.parseInt(data[1]), data[0]);
                break;
        }
    }

    private void setLanguage(Update update, LanguageSet set) {
        languageSet = set;
        sendAnswer(update, languageSet.getCHANGE_SUCCESS());
    }

    private void sendMusic(Update update, String title) throws TelegramApiException, NoFindMusicException {
        MusicGetter musicGetter;
        try {
            sendAnswer(update, languageSet.getLOADING_MESSAGE());
            musicGetter = new ZaycevGetter();
            sendFound(update, title, musicGetter);
        } catch (NoFindMusicException e) {
            musicGetter = new YoutubeGetter();
            sendFound(update, title, musicGetter);
        }
    }

    private void sendFound(Update update, String title, MusicGetter musicGetter) throws NoFindMusicException, TelegramApiException {
        try {
            String url = musicGetter.getURLToDownloadMusic(title);
            InputStream inputStream = new URL(url).openStream();
            InputFile file = new InputFile(inputStream, title);

            execute(SendAudio.builder()
                    .chatId(getUpdateChatId(update))
                    .audio(file)
                    .build()
            );
        } catch (IOException e) {
            sendAnswer(update, languageSet.getCANT_DOWNLOAD());
        }
    }

    @Override
    public String getBotUsername() {
        return "@SongRidersBot";
    }

    @Override
    public String getBotToken() {
        return "5031402619:AAHmcu3l0jCuuLph2D9_z6svNu6hryQbXNA";
    }
}