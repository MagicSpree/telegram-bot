package bot;

public enum Command {
    NONE(""),
    START("/start"),
    TRACK("/track"),
    ARTIST("/artist"),
    LINK("/link"),
    LANG("/lang");

    final String value;

    Command(String s) {
        this.value = s;
    }

    public static Command fromString(String text) {
        for (Command b : values()) {
            if (b.value.equalsIgnoreCase(text)) {
                return b;
            }
        }
        return NONE;
    }
}
