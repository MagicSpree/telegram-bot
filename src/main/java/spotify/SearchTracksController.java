package spotify;

import org.apache.log4j.Logger;
import se.michaelthelin.spotify.SpotifyApi;
import se.michaelthelin.spotify.enums.ModelObjectType;
import se.michaelthelin.spotify.model_objects.specification.Track;
import spotify.exception.SpotifyAPIException;
import spotify.searchers.AlbumSearcher;
import spotify.searchers.ArtistSearcher;
import spotify.searchers.PlayListSearcher;
import spotify.searchers.TrackSearcher;


/**
 * Класс-контроллер, отвечающий за поиск треков в Spotify
 *
 * @author Chelnokov Egor
 * @version 1.0
 */
public class SearchTracksController {

    protected static final Logger log = Logger.getLogger(SearchTracksController.class);
    private static final SpotifyApi spotifyApi = new SpotifyApi.Builder()
            .setAccessToken(SpotifyAuthorizator.getAccessToken())
            .build();


    /**
     * возвращает массив треков по полученному запросу
     *
     * @param query  запрос формата "команда поисковый запрос"
     * @param offset постраничный отступ
     * @return массив с первыми найденными треками в количестве 1-10
     * @throws SpotifyAPIException исключение, связанное с работой Spotify
     */
    public static Track[] search(String query, int offset) throws SpotifyAPIException {
        Track[] found = new Track[0];
        String type = getTypeFromQuery(query);
        switch (type) {
            case "track":
                found = new TrackSearcher(spotifyApi).searchByTitle(query.replace("/track ", ""), offset);
                break;
            case "artist":
                found = new ArtistSearcher(spotifyApi).searchByArtist(query.replace("/artist ", ""));
                break;
            case "link":
                found = searchByLink(query.replace("/link ", ""));
                break;
        }
        return found;
    }

    /**
     * Возвращает массив найденных треков по ссылке из Spotify
     *
     * @param link Spotify ссылка
     * @return массив с найденными треками
     * @throws SpotifyAPIException исключение, связанное с работой Spotify
     */
    public static Track[] searchByLink(String link) throws SpotifyAPIException {
        String type = getTypeFromLink(link);
        String id = getIdFromLink(link);
        Track[] found = new Track[0];
        switch (ModelObjectType.keyOf(type)) {
            case TRACK:
                Track track = new TrackSearcher(spotifyApi).getTrackById(id);
                found = new Track[]{track};
                break;
            case ARTIST:
                ArtistSearcher artistSearcher = new ArtistSearcher(spotifyApi);
                found = artistSearcher.getArtistTopTracks(id);
                break;
            case PLAYLIST:
                PlayListSearcher playListSearcher = new PlayListSearcher(spotifyApi);
                found = playListSearcher.getTracksFromPlaylist(id);
                break;
            case ALBUM:
                AlbumSearcher albumSearcher = new AlbumSearcher(spotifyApi);
                found = albumSearcher.getAlbumTracks(id);
                break;
        }
        return found;
    }

    /**
     * Возвращает строку с типом искомого результата из запроса
     *
     * @param query запрос для поиска
     * @return строка, содержащая тип
     */
    private static String getTypeFromQuery(String query) {
        String type = query.split(" ")[0];
        return type.substring(1);
    }

    public static Track searchTrackById(String id) {
        return new TrackSearcher(spotifyApi).getTrackById(id);
    }

    /**
     * Возвращает строку с ID искомого результата из ссылки
     *
     * @param link ссылка
     * @return строка с результатом
     */
    public static String getIdFromLink(String link) {
        String cut = link.replace("https://open.spotify.com/" + getTypeFromLink(link), "");
        return cut.substring(1, cut.indexOf("?"));
    }

    /**
     * Возвращает строку с типом искомого результата из ссылки
     *
     * @param link ссылка
     * @return строка с результатом
     */
    public static String getTypeFromLink(String link) {
        String cut = link.replace("https://open.spotify.com/", "");
        return cut.substring(0, cut.indexOf("/"));
    }

}