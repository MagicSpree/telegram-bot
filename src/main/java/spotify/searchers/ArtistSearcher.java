package spotify.searchers;

import com.neovisionaries.i18n.CountryCode;
import se.michaelthelin.spotify.SpotifyApi;
import se.michaelthelin.spotify.model_objects.specification.Artist;
import se.michaelthelin.spotify.model_objects.specification.Track;
import se.michaelthelin.spotify.requests.data.search.simplified.SearchArtistsRequest;
import spotify.exception.SpotifyAPIException;

import java.util.concurrent.ExecutionException;

/**
 * Класс, реализующий поиск треков по данным артиста
 *
 * @author Chelnokov Egor
 * @version 1.0
 */
public class ArtistSearcher extends AbstractSearcher {

    public ArtistSearcher(SpotifyApi spotifyApi) {
        super(spotifyApi);
    }

    /**
     * Возвращает найденные треки по имени/фрагменту имени артиста
     * @param query поисковый запрос
     * @return массив с популярными треками артиста
     * @throws SpotifyAPIException исключение, связанное с работой Spotify
     */
    public Track[] searchByArtist(String query) throws SpotifyAPIException {
        try {
        SearchArtistsRequest searchArtistsRequest = spotifyApi.searchArtists(query)
                .limit(1)
                .includeExternal("audio")
                .build();
            Artist artist = searchArtistsRequest.execute().getItems()[0];
            String artistId = artist.getId();
            return getArtistTopTracks(artistId);
        } catch (Exception e) {
            log.error(e);
            throw new SpotifyAPIException(e);
        }
    }

    /**
     * Возвращает популярные треки артиста по его ID
     * @param artistId ID артиста в Spotify
     * @return массив с популярными треками артиста
     * @throws SpotifyAPIException исключение, связанное с работой Spotify
     */
    public Track[] getArtistTopTracks(String artistId) throws SpotifyAPIException {
        try {
            return spotifyApi.getArtistsTopTracks(artistId, CountryCode.RU)
                    .build()
                    .executeAsync()
                    .get();
        } catch (InterruptedException | ExecutionException e) {
            log.error(e);
            throw new SpotifyAPIException(e);
        }
    }

}
