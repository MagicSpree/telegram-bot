package spotify.searchers;

import org.apache.log4j.Logger;
import se.michaelthelin.spotify.SpotifyApi;
import spotify.SpotifyAuthorizator;

/**
 * Абстрактный класс поисковика треков, получающий Spotify Api
 */
public abstract class AbstractSearcher {

    protected SpotifyApi spotifyApi;
    protected static final Logger log = Logger.getLogger(AbstractSearcher.class);

    public AbstractSearcher(SpotifyApi spotifyApi) {
        this.spotifyApi = spotifyApi;
    }

}
