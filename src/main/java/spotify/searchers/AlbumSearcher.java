package spotify.searchers;

import se.michaelthelin.spotify.SpotifyApi;
import se.michaelthelin.spotify.model_objects.specification.Track;
import spotify.exception.SpotifyAPIException;

import java.util.Arrays;
import java.util.concurrent.ExecutionException;
/**
 * Класс, реализующий поиск треков по данным альбома
 *
 * @author Chelnokov Egor
 * @version 1.0
 */
public class AlbumSearcher extends AbstractSearcher {
    public AlbumSearcher(SpotifyApi spotifyApi) {
        super(spotifyApi);
    }

    /**
     * возвращает массив треков из найденного альбома по его ID
     * @param albumId ID альбома
     * @return массив с найденными треками
     * @throws SpotifyAPIException исключение, связанное с работой Spotify
     */
    public Track[] getAlbumTracks(String albumId) throws SpotifyAPIException {
        try {
            return Arrays.stream(spotifyApi.getAlbumsTracks(albumId)
                    .build()
                    .executeAsync()
                    .get()
                    .getItems())
                    .map(t-> new TrackSearcher(spotifyApi).getTrackById(t.getId()))
                    .toArray(Track[]::new);
        } catch (InterruptedException | ExecutionException e) {
            log.error(e);
            throw new SpotifyAPIException(e);
        }
    }
}
