package spotify.searchers;

import se.michaelthelin.spotify.SpotifyApi;
import se.michaelthelin.spotify.model_objects.specification.ArtistSimplified;
import se.michaelthelin.spotify.model_objects.specification.Paging;
import se.michaelthelin.spotify.model_objects.specification.Track;
import se.michaelthelin.spotify.requests.data.search.simplified.SearchTracksRequest;
import spotify.SearchTracksController;
import spotify.SpotifyAuthorizator;
import spotify.exception.SpotifyAPIException;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

/**
 * Класс, реализующий поиск треков в Spotify
 *
 * @author Chelnokov Egor
 * @version 1.0
 */
public class TrackSearcher extends AbstractSearcher {

    public TrackSearcher(SpotifyApi spotifyApi) {
        super(spotifyApi);
    }

    /**
     * Возвращает трек по его ID
     *
     * @param trackId ID трека
     * @return найденный трек
     */
    public Track getTrackById(String trackId) {
        Track track = new Track.Builder().build();
        try {
            track = Optional.of(spotifyApi.getTrack(trackId)
                    .build()
                    .executeAsync()
                    .get()).orElseThrow(SpotifyAPIException::new);
        } catch (InterruptedException | ExecutionException | SpotifyAPIException e) {
            log.error("Ошибка получения данных о треке. Причина: " + e.getMessage());
        }
        return track;
    }

    /**
     * возвращает полное наименование трека по его ID
     *
     * @param trackId ID трека
     * @return полное наименование трека
     */
    public static String getTrackTitle(String trackId) {
        Track track = SearchTracksController.searchTrackById(trackId);
        return getArtistsName(track) + " - " + track.getName();
    }

    public Track[] searchByTitle(String query, int offset) throws SpotifyAPIException {
        SearchTracksRequest searchTracksRequest = spotifyApi.searchTracks(query)
                .limit(10)
                .offset(offset)
                .includeExternal("audio")
                .build();

        final CompletableFuture<Paging<Track>> pagingFuture = searchTracksRequest.executeAsync();
        try {
            final Paging<Track> trackPaging = pagingFuture.get();
            return trackPaging.getItems();
        } catch (InterruptedException | ExecutionException e) {
            log.error(e);
            throw new SpotifyAPIException(e);
        }
    }
    /**
     * возвращает строку, содержащую всех исполнителей данного трека
     *
     * @param track исходный трек
     * @return строка с именами исполнителей трека
     */
    static String getArtistsName(Track track) {
        List<ArtistSimplified> artistList = Arrays.stream(track.getArtists()).collect(Collectors.toList());
        StringBuilder artists = new StringBuilder();
        artistList.forEach(artist -> artists.append(artist.getName()).append(", "));
        return artists.substring(0, artists.lastIndexOf(","));
    }
}
