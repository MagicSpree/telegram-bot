package spotify.searchers;

import se.michaelthelin.spotify.SpotifyApi;
import se.michaelthelin.spotify.model_objects.specification.Track;
import spotify.exception.SpotifyAPIException;

import java.util.Arrays;
import java.util.concurrent.ExecutionException;

/**
 * Класс, реализующий поиск треков по данным артиста
 *
 * @author Chelnokov Egor
 * @version 1.0
 */
public class PlayListSearcher extends AbstractSearcher {

    public PlayListSearcher(SpotifyApi spotifyApi) {
        super(spotifyApi);
    }

    /**
     * Возвращает массив с треками из плейлиста по его ID
     * @param playListId ID плейлиста
     * @return массив с треками из найденного плейлиста
     * @throws SpotifyAPIException исключение, связанное с работой Spotify
     */
    public Track[] getTracksFromPlaylist(String playListId) throws SpotifyAPIException {
        try {
            return Arrays.stream(spotifyApi.getPlaylist(playListId).build()
                    .executeAsync()
                    .get()
                    .getTracks()
                    .getItems())
                    .map(t-> new TrackSearcher(spotifyApi).getTrackById(t.getTrack().getId()))
                    .toArray(Track[]::new);
        } catch (InterruptedException | ExecutionException e) {
            log.error(e);
            throw new SpotifyAPIException(e);
        }
    }
}
