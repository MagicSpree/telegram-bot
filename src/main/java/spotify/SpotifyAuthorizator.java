package spotify;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import spotify.exception.SpotifyAPIException;
import musicDownload.service.SpotifyService;

import java.io.IOException;

/**
 * Класс описывающий получения токена для работы с API Spotify
 * @author Dinar Abubekerov
 * @version 1.0
 */

public class SpotifyAuthorizator {

    /** Поле accessToken'а */
    private static String accessToken;

    /** Метод для получения значения поля accessToken */
    public static String getAccessToken() {
        return accessToken;
    }

    /** Метод для установки значения поля accessToken */
    public static void init() throws IOException, SpotifyAPIException {
        accessToken = getToken();
    }

    /** Метод для получения accessToken через Spotify API
     * @return возвращает accessToken
     * @throws SpotifyAPIException если не удалось осуществить запрос к Spotify API
     * @throws IOException если не удалось прочитать результат запроса
     */
    private static String getToken() throws SpotifyAPIException, IOException {
        SpotifyService spotifyService = new SpotifyService();
        Call<ResponseBody> videoCall = spotifyService.getService().getAccessTokenSpotify("client_credentials");
        Response<ResponseBody> responseS = videoCall.execute();
        if (!responseS.isSuccessful() || responseS.body() == null) {
            throw new SpotifyAPIException();
        }
        String jsonUrl = responseS.body().string();
        JsonObject jsonObject = JsonParser.parseString(jsonUrl).getAsJsonObject();
        return jsonObject.get("access_token").getAsString();
    }

}
