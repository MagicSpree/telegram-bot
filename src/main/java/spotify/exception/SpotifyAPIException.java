package spotify.exception;

public class SpotifyAPIException extends Exception {

    public SpotifyAPIException(Exception e) {
        super("Ошибка получения данных из Spotify. Причина: " + e);
    }

    public SpotifyAPIException() {
        super("Ошибка получения данных из Spotify");
    }
}
