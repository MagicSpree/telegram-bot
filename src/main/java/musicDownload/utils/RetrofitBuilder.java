package musicDownload.utils;

import com.google.gson.GsonBuilder;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import musicDownload.model.Music;

/**
 * Класс для создания объекта класса Retrofit
 * @author Dinar Abubekerov
 * @version 1.0
 * @see Retrofit
 */
public class RetrofitBuilder {

    /**
     * Метод для создания объекта класса Retrofit
     *
     * @param baseUrl основной URL
     * @param deserializer  десериализатор для обработки JSON
     * @return Объект класса Retrofit
     */
    public static Retrofit getRetrofit(String baseUrl, Object deserializer) {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(
                        new GsonBuilder()
                                .registerTypeAdapter(Music.class, deserializer)
                                .create()
                ))
                .build();
    }
}
