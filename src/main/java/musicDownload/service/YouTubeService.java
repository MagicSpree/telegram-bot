package musicDownload.service;

import musicDownload.api.NetworkService;
import musicDownload.api.Service;
import musicDownload.deserializer.YouTubeVideoDeserializer;
import musicDownload.utils.RetrofitBuilder;

/**
 * Класс описывающий получения объекта/службы, который позволяет отправлять запросы к YouTube Data API
 *
 * @author Dinar Abubekerov
 * @version 1.0
 * @see Service
 * @see NetworkService
 */

public class YouTubeService implements Service {

    /**
     * Поле ссылки для обращения к YouTube Data API
     */
    private static final String YOUTUBE_URL_API = "https://youtube.googleapis.com/youtube/v3/";

    /**
     * Метод для получения объекта/службы по работе с YouTube Data API
     *
     * @return Объект/служба
     */
    @Override
    public NetworkService getService() {
        return RetrofitBuilder.getRetrofit(YOUTUBE_URL_API, new YouTubeVideoDeserializer()).create(NetworkService.class);
    }
}
