package musicDownload.service;

import musicDownload.api.NetworkService;
import musicDownload.utils.RetrofitBuilder;
import musicDownload.api.Service;
import musicDownload.deserializer.ZaycevMusicDeserializer;

/**
 * Класс описывающий получения объекта/службы, который позволяет отправлять запросы к Spotify API
 * @author Dinar Abubekerov
 * @version 1.0
 * @see NetworkService
 */

public class SpotifyService implements Service {

    /** Поле ссылки для обращения к Spotify API */
    private static final String SPOTIFY_URL_API = "https://accounts.spotify.com/";

    /** Метод для получения объекта/службы по работе с Spotify API
     * @return возвращает объект/службу
     */
    @Override
    public NetworkService getService() {
        return RetrofitBuilder.getRetrofit(SPOTIFY_URL_API, new ZaycevMusicDeserializer()).create(NetworkService.class);
    }
}
