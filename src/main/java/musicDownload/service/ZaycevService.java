package musicDownload.service;

import musicDownload.api.NetworkService;
import musicDownload.utils.RetrofitBuilder;
import musicDownload.api.Service;
import musicDownload.deserializer.ZaycevMusicDeserializer;

/**
 * Класс описывающий получения объекта/службы, который позволяет отправлять запросы к Zaycev API
 *
 * @author Dinar Abubekerov
 * @version 1.0
 * @see Service
 * @see NetworkService
 */

public class ZaycevService implements Service {

    /**
     * Поле ссылки для обращения к Zaycev API
     */
    private static final String ZAYCEV_URL_API = "https://api.zaycev.net/external/";

    /**
     * Метод для получения объекта/службы по работе с Zaycev API
     *
     * @return Объект/служба для работы с API
     */
    @Override
    public NetworkService getService() {
        return RetrofitBuilder.getRetrofit(ZAYCEV_URL_API, new ZaycevMusicDeserializer()).create(NetworkService.class);
    }
}
