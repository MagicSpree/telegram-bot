package musicDownload.deserializer;

import com.google.gson.*;
import musicDownload.model.Music;

import java.lang.reflect.Type;
import java.util.Locale;

/**
 * Класс позволяющий обрабатывать JSON ответ полученный от YouTube Data API
 *
 * @author Dinar Abubekerov
 * @version 1.0
 */
public class YouTubeVideoDeserializer implements JsonDeserializer<Music> {

    //    /**
//     * Метод для получения информации о видео по названию через YouTube Data API
//     *
//     * @param jsonElement                JSON объект
//     * @param type                       песня
//     * @param jsonDeserializationContext ключ для получения данных через API
//     * @return Вызов внутри которого находится объект класса Music
//     */
    @Override
    public Music deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext)
            throws JsonParseException {

        JsonObject jsonObject = jsonElement.getAsJsonObject();
        Music music = new Music();

        JsonArray itemsArray = jsonObject.getAsJsonArray("items");
        JsonElement item = itemsArray.get(0);
        JsonElement idTag = item.getAsJsonObject().get("id");
        music.setId(idTag.getAsJsonObject().get("videoId").getAsString());

        JsonElement snippetTag = item.getAsJsonObject().get("snippet");
        String title = snippetTag.getAsJsonObject().get("title").getAsString();
        splitTitle(title, music);

        return music;
    }

    /**
     * Метод для разделения оглавления видеозаписи по автору и названию
     *
     * @param title оглавление записи
     * @param music песня
     */
    private void splitTitle(String title, Music music) {
        String track;
        String[] words = title.split("-");
        if (words.length > 1) {
            String author = words[0].trim().toLowerCase(Locale.ROOT);
            track = words[1].trim().toLowerCase(Locale.ROOT);
            music.setArtistName(author);
        } else {
            track = words[0].trim().toLowerCase(Locale.ROOT);
        }
        music.setTrackName(track);
    }
}
