package musicDownload.deserializer;

import com.google.gson.*;
import musicDownload.model.Music;

import java.lang.reflect.Type;

/**
 * Класс позволяющий обрабатывать JSON ответ полученный от Zaycev API
 *
 * @author Dinar Abubekerov
 * @version 1.0
 */
public class ZaycevMusicDeserializer implements JsonDeserializer<Music> {

    @Override
    public Music deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext)
            throws JsonParseException {

        JsonObject jsonObject = jsonElement.getAsJsonObject();
        Music music = new Music();

        JsonArray tracksArray = jsonObject.getAsJsonArray("tracks");
        JsonElement track = tracksArray.get(0);

        music.setArtistName(track.getAsJsonObject().get("artistName").getAsString());
        music.setId(track.getAsJsonObject().get("id").getAsString());
        music.setTrackName(track.getAsJsonObject().get("track").getAsString());

        return music;
    }
}