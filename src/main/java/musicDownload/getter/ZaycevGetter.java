package musicDownload.getter;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import musicDownload.service.ZaycevService;
import musicDownload.exception.NoFindMusicException;
import musicDownload.getter.acessToken.AccessTokenApi;
import musicDownload.model.Music;

import java.io.IOException;
import java.util.Locale;

/**
 * Класс описывающий получения ссылки для скачивания песни
 *
 * @author Dinar Abubekerov
 * @version 1.0
 * @see MusicGetter
 * @see Music
 */
public class ZaycevGetter extends MusicGetter {

    /**
     * Поле accessToken для подключения к Zaycev API
     */
    private final String ZAYCEV_API_KEY;

    /**
     * Поле объекта ZaycevService для работы с Zaycev API
     */
    private final ZaycevService zaycevService = new ZaycevService();

    /**
     * Конструктор - создание нового объекта с определенным значением
     */
    public ZaycevGetter() throws NoFindMusicException {
        ZAYCEV_API_KEY = getAccessToken();
    }

    /**
     * Метод позволяющий получить ссылки для скачивания музыки
     *
     * @param nameMusic название песни
     * @return Ссылка для скачивания песни
     * @throws NoFindMusicException если не удалось получить ссылку для скачивания
     */
    @Override
    public String getURLToDownloadMusic(String nameMusic) throws NoFindMusicException {
        try {
            Call<Music> zaycevMusicCall = zaycevService.getService().getMusicInformation(nameMusic, ZAYCEV_API_KEY);
            Music music = getMusicObject(zaycevMusicCall);

            if (music == null) {
                log.error("Не удалось извлечь данные из запроса");
                throw new NoFindMusicException("Не удалось извлечь данные из запроса");
            }

            String trackNameFormZaycev = music.getArtistName() + " - " + music.getTrackName();
            if (constrain(trackNameFormZaycev, nameMusic)) {
                return getURLMusicFromZaycev(music.getId());
            }

            log.info("Музыка в источнике ZaycevNET не найдена");
            throw new NoFindMusicException();

        } catch (Exception e) {
            log.error("Не удалочь получить ссылку на скачивание. Причина: " + e.getMessage());
            throw new NoFindMusicException("Не удалочь получить ссылку на скачивание. Причина: " + e.getMessage());
        }
    }

    /**
     * Метод для сравнения полученного названия песни и тем, что ищет пользователь
     *
     * @param userMusicName       название песни, которую хочет найти пользователь
     * @param musicNameFromSource название песни, которая была получения из запроса
     * @return Ссылка для скачивания песни
     */
    @Override
    protected boolean constrain(String userMusicName, String musicNameFromSource) {
        String[] words = musicNameFromSource.split("-");
        String authors = words[0].trim().toLowerCase(Locale.ROOT);
        String track = words[1].trim().toLowerCase(Locale.ROOT);
        userMusicName = userMusicName.toLowerCase(Locale.ROOT);
        return userMusicName.contains(track) && userMusicName.contains(authors);
    }

    /**
     * Метод для получения ссылки из полученного запроса от Zaycev API
     *
     * @param trackId идентификатор песни
     * @return Ссылка для скачивания песни
     * @throws IOException если не удалось прочитать полученный запрос
     */
    private String getURLMusicFromZaycev(String trackId) throws IOException {
        Call<ResponseBody> urlCall = zaycevService.getService().getURLForMusicDownload(trackId, ZAYCEV_API_KEY);
        Response<ResponseBody> responseS = urlCall.execute();

        if (!responseS.isSuccessful() || responseS.body() == null) {
            log.error("Не удалось получить информацию из запроса");
            throw new IOException("Не удалось получить информацию из запроса");
        }

        String jsonUrl = responseS.body().string();
        JsonObject jsonObject = JsonParser.parseString(jsonUrl).getAsJsonObject();
        return jsonObject.get("url").getAsString();
    }

    /**
     * Метод для получения ссылки из полученного запроса от Zaycev API
     *
     * @return Ссылка для скачивания песни
     * @throws NoFindMusicException если не удалось получить accessToken
     */
    private String getAccessToken() throws NoFindMusicException {
        AccessTokenApi tokenApi = new AccessTokenApi();
        return tokenApi.getAccessTokenFromAPI();
    }
}
