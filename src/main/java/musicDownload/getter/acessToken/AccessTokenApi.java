package musicDownload.getter.acessToken;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import okhttp3.ResponseBody;
import org.apache.log4j.Logger;
import retrofit2.Call;
import retrofit2.Response;
import musicDownload.service.ZaycevService;
import musicDownload.exception.NoFindMusicException;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Класс описывающий получения accessToken для работы с Zaycev API
 *
 * @author Dinar Abubekerov
 * @version 1.0
 */
public class AccessTokenApi {

    /**
     * Объект класса для осуществления логирования
     */
    private static final Logger log = Logger.getLogger(AccessTokenApi.class);

    /**
     * Поле константы для получения токена разработчика
     */
    private final String CONST_VERSION = "Fh6w165Fmub";

    /**
     * Поле объекта ZaycevService для работы с Zaycev API
     */
    private final ZaycevService zaycevService = new ZaycevService();

    /**
     * Метод для получения токена разработчика (для работы с API)
     *
     * @return Токен
     * @throws NoFindMusicException если не удалось получить токен
     */
    public String getAccessTokenFromAPI() throws NoFindMusicException {
        try {
            String helloToken = getHelloToken();
            String md5Hash = getMD5Hash(helloToken + CONST_VERSION);
            return getAccessToken(helloToken, md5Hash);
        } catch (IOException | NoSuchAlgorithmException e) {
            log.error("Не удалось получить accessToken из ZaycevNET. Причина: " + e);
            throw new NoFindMusicException("Не удалось получить accessToken из ZaycevNET. Причина: " + e);
        }
    }

    /**
     * Метод для получения токена разработчика (для работы с API)
     *
     * @param helloToken "приветственный" токен
     * @param hash       хеш-строка полученная из ("приветственный" токен + "константа версии")
     * @return Токен
     * @throws IOException если не удалось прочитать "тело" ответа
     */
    private String getAccessToken(String helloToken, String hash) throws IOException {
        Call<ResponseBody> accessTokenCall = zaycevService.getService().getAccessTokenZaycevNET(helloToken, hash);
        return getBodyTokenRequest(accessTokenCall);
    }

    /**
     * Метод для получения "приветственного" токена
     *
     * @return Токен
     * @throws IOException если не удалось прочитать "тело" ответа
     */
    private String getHelloToken() throws IOException {
        Call<ResponseBody> helloCall = zaycevService.getService().getHelloTokenZaycevNET();
        return getBodyTokenRequest(helloCall);
    }

    /**
     * Метод для получения "тела" ответа
     *
     * @param postCall объект для получения "тела" ответа
     * @return Токен
     * @throws IOException если не удалось прочитать "тело" ответа
     */
    private String getBodyTokenRequest(Call<ResponseBody> postCall) throws IOException {
        Response<ResponseBody> responseS = postCall.execute();
        if (!responseS.isSuccessful() || responseS.body() == null) {
            throw new IOException("Не удалось прочитать данные из запроса");
        }
        return getTokenFromJson(responseS.body());
    }

    /**
     * Метод для получения токена из Json ответа
     *
     * @param responseBody "тело" полученное из запроса
     * @return Токен
     * @throws IOException если не удалось прочитать "тело" ответа
     */
    private String getTokenFromJson(ResponseBody responseBody) throws IOException {
        String jsonUrl = responseBody.string();
        JsonObject jsonObject = JsonParser.parseString(jsonUrl).getAsJsonObject();
        return jsonObject.get("token").getAsString();
    }

    /**
     * Метод для получения hash-функции MD5 из строки
     *
     * @param strForHash строка для хеширования
     * @return Преобразованная в hash строка
     * @throws NoSuchAlgorithmException если не удалось найти алгоритм для хеширования
     */
    private String getMD5Hash(String strForHash) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(strForHash.getBytes());
        byte[] digest = md.digest();
        return bytesToHex(digest);
    }

    /**
     * Метод для преобразования строки в hex формат
     *
     * @param bytes массив байт
     * @return Преобразованная в hex формат строка
     */
    private String bytesToHex(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (byte b : bytes) {
            sb.append(String.format("%02x", b));
        }
        return sb.toString();
    }

}
