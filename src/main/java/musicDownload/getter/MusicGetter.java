package musicDownload.getter;

import org.apache.log4j.Logger;
import retrofit2.Call;
import retrofit2.Response;
import musicDownload.exception.NoFindMusicException;
import musicDownload.model.Music;

import java.io.IOException;

/**
 * Абстактный класс описывающий получения ссылки для скачивания песни
 *
 * @author Dinar Abubekerov
 * @version 1.0
 * @see MusicGetter
 * @see Music
 */
public abstract class MusicGetter {

    protected static final Logger log = Logger.getLogger(MusicGetter.class);

    /**
     * Абстрактный метод, позволяющий получить ссылки для скачивания музыки
     *
     * @param nameMusic название песни
     * @return Ссылка для скачивания песни
     * @throws NoFindMusicException если не удалось получить ссылку для скачивания
     */
    public abstract String getURLToDownloadMusic(String nameMusic) throws NoFindMusicException;

    /**
     * Абстрактный метод для сравнения полученного названия песни и тем, что ищет пользователь
     *
     * @param userMusicName       название песни, которую хочет найти пользователь
     * @param musicNameFromSource название песни, которая была получения из запроса
     * @return Ссылка для скачивания песни
     */
    protected abstract boolean constrain(String userMusicName, String musicNameFromSource);

    /**
     * Абстрактный метод, позволяющий получить ссылки для скачивания музыки
     *
     * @param musicCall объект для осуществления динамического вызова службы
     * @return Объект Music
     * @throws IOException если не удалось прочитать результат запроса
     */
    protected Music getMusicObject(Call<Music> musicCall) throws IOException {
        Response<Music> response = musicCall.execute();
        if (!response.isSuccessful() || response.body() == null) {
            throw new IOException("Не удалось получить информацию из запроса");
        }
        return response.body();
    }
}
