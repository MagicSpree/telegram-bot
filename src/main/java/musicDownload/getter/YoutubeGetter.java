package musicDownload.getter;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import retrofit2.Call;
import musicDownload.service.YouTubeService;
import musicDownload.exception.NoFindMusicException;
import musicDownload.model.Music;

import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Locale;

/**
 * Класс описывающий получения ссылки для скачивания песни
 *
 * @author Dinar Abubekerov
 * @version 1.0
 * @see MusicGetter
 * @see Music
 */
public class YoutubeGetter extends MusicGetter {

    /**
     * Поле определяющее, что у полученной песни имеется автор
     */
    private static final int AUTHOR_AND_TRACK = 2;

    /**
     * Метод позволяющий получить ссылки для скачивания музыки
     *
     * @param nameMusic название песни
     * @return Ссылка для скачивания песни
     * @throws NoFindMusicException если не удалось получить ссылку для скачивания
     */
    @Override
    public String getURLToDownloadMusic(String nameMusic) throws NoFindMusicException {
        try {
            String youTubeApiKey = "AIzaSyA-e1LPfeN0VzlHuKwZAMscIxZM5CwkPiU";
            YouTubeService youTubeService = new YouTubeService();
            Call<Music> videoCall = youTubeService.getService().getVideoInformation(nameMusic, youTubeApiKey);
            Music music = getMusicObject(videoCall);
            if (music == null) {
                throw new NoFindMusicException("Не удалось получить результат запроса ");
            }
            return generateURLToDownloadFromYoutube(nameMusic, music.getId());
        } catch (Exception e) {
            log.error("Не удалоcь получить ссылку на скачивание. Причина: " + e.getMessage());
            throw new NoFindMusicException("Не удалочь получить ссылку на скачивание. Причина: " + e.getMessage());
        }
    }

    /**
     * Метод для сравнения полученного названия песни и тем, что ищет пользователь
     *
     * @param userMusicName       название песни, которую хочет найти пользователь
     * @param musicNameFromSource название песни, которая была получения из запроса
     * @return Ссылка для скачивания песни
     */
    @Override
    protected boolean constrain(String userMusicName, String musicNameFromSource) {
        String[] wordsFromUser = musicNameFromSource.split("-");
        String authors = wordsFromUser[0].trim().toLowerCase(Locale.ROOT);
        String track = wordsFromUser[1].trim().toLowerCase(Locale.ROOT);

        String author = authors.split("[,&]")[0];

        userMusicName = userMusicName.toLowerCase(Locale.ROOT);
        String[] wordsFromAPI = userMusicName.split("-");
        if (wordsFromAPI.length == AUTHOR_AND_TRACK) {
            return userMusicName.contains(track) && userMusicName.contains(author);
        }
        return userMusicName.contains(track);
    }

    /**
     * Метод для генерации ссылки из полученного запроса от YouTube API
     *
     * @param nameMusic название песни
     * @param idMusic   идентификатор песни
     * @return Ссылка для скачивания песни
     * @throws IOException          если не удалось прочитать полученный запрос
     * @throws NoFindMusicException если не удалось получить ссылку для скачивания
     */
    private String generateURLToDownloadFromYoutube(String nameMusic, String idMusic) throws IOException, NoFindMusicException {
        String domainForDownload = "https://api.vevioz.com/";
        String urlButton = String.format(domainForDownload + "?v=%s&type=mp3&bitrate=320", idMusic);
        Document doc = Jsoup.connect(urlButton).get();

        Element mp3InfoDownload = doc.getElementById("mediaDownload");
        Elements mp3InfoParent = doc.getElementsByClass("media-parent");

        if (mp3InfoDownload == null) {
            log.error("Не удалось получить информацию из запроса");
            throw new IOException("Не удалось получить информацию из запроса");
        }
        String mp3Title = mp3InfoDownload.attr("data-yt-title");
        String mp3Tag = mp3InfoParent.attr("data-mp3-tag");

        if (constrain(mp3Title, nameMusic)) {
            String musicName = URLEncoder.encode(nameMusic, StandardCharsets.UTF_8.name());
            return domainForDownload + "download/" + mp3Tag + "/" + musicName + ".mp3";
        }
        log.info("Музыка в источнике YouTube не найдена");
        throw new NoFindMusicException();
    }
}
