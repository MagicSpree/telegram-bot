package musicDownload.model;

/**
 * Класс описывающий объект "Музыка"/"Песня"
 * @author Dinar Abubekerov
 * @version 1.0
 */

public class Music {

    /** Поле идентификатор песни */
    private String id;

    /** Поле исполнителя песни */
    private String artistName;

    /** Поле названия песни */
    private String trackName;

    /** Метод для получения идентификатора песни
     * @return Идентификатор
     */
    public String getId() {
        return id;
    }

    /** Метод для установки идентификатора песни
     * @param id идентификатор
     */
    public void setId(String id) {
        this.id = id;
    }

    /** Метод для получения идентификатора песни
     * @return Имя исполнителя
     */
    public String getArtistName() {
        return artistName;
    }

    /** Метод для установки имени исполнителя
     * @param artistName исполнитель
     */
    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    /** Метод для получения названия песни
     * @return Название песни
     */
    public String getTrackName() {
        return trackName;
    }

    /** Метод для установки названия песни
     * @param trackName название песни
     */
    public void setTrackName(String trackName) {
        this.trackName = trackName;
    }
}
