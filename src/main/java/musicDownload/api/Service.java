package musicDownload.api;

/**
 * Интерфейс позволяющий получать объект/службу для работы с различными API
 *
 * @author Dinar Abubekerov
 * @version 1.0
 */
public interface Service {

    /**
     * Метод для получения объекта/службы для работы с различными API
     *
     * @return Вызов внутри которого находится объект класса Music
     */
    NetworkService getService();
}
