package musicDownload.api;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.*;
import musicDownload.model.Music;

/**
 * Интерфейс позволяющий осуществлять запросы к различным API
 *
 * @author Dinar Abubekerov
 * @version 1.0
 */
public interface NetworkService {

    /**
     * Метод для получения информации о видео по названию через YouTube Data API
     *
     * @param nameMusic Название песни
     * @param api       ключ для получения данных через API
     * @return Объект для получения объекта класса Music
     */
    @GET("search?units=metric&part=snippet&maxResults=1&fields=items(id/videoId, snippet/title)")
    Call<Music> getVideoInformation(
            @Query("q") String nameMusic,
            @Query("key") String api
    );

    /**
     * Метод для получение информации о песне через Zaycev API
     *
     * @param nameMusic название песни
     * @param api       ключ для получения данных через API
     * @return Объект для получения объекта класса Music
     */
    @GET("search")
    Call<Music> getMusicInformation(
            @Query("query") String nameMusic,
            @Query("access_token") String api
    );

    /**
     * Метод для получение ссылки для скачивания песни через Zaycev API
     *
     * @param id  идентификатор песни
     * @param api ключ для получения данных через API
     * @return Объект для получения результата запроса (JSON)
     */
    @GET("track/{trackId}/download")
    Call<ResponseBody> getURLForMusicDownload(
            @Path("trackId") String id,
            @Query("access_token") String api
    );

    /**
     * Метод для получение токена разработчика (для работы с API)
     *
     * @param helloToken "приветственный" токен
     * @param hash       хеш-строка полученная из ("приветственный" токен + "константа версии")
     * @return Объект для получения результата запроса (JSON)
     */
    @GET("auth")
    Call<ResponseBody> getAccessTokenZaycevNET(
            @Query("code") String helloToken,
            @Query("hash") String hash
    );

    /**
     * Метод для получение "приветственного" токена
     *
     * @return Объект для получения результата запроса (JSON)
     */
    @GET("hello")
    Call<ResponseBody> getHelloTokenZaycevNET();

    /**
     * Метод для получение Spotify API токена
     *
     * @param grantType (константа: client_credentials)
     * @return Объект для получения результата запроса (JSON)
     */
    @FormUrlEncoded
    @Headers({"Content-Type: application/x-www-form-urlencoded",
            "Authorization: Basic YTg5ZjFlZjM4ZTczNDYzODkwNTQxZDAzYmYxODFkOWE6M2E3YTYxYjYzYzY0NDVlMGI4OTU0YzQyZGJjNTVmYjY="})
    @POST("api/token")
    Call<ResponseBody> getAccessTokenSpotify(
            @Field("grant_type") String grantType
    );
}
