package musicDownload.exception;

/**
 * Это исключение возникает, когда возникли ошибки при получении ссылки
 * для скачивания музыки из источника
 *
 * @author Dinar Abubekerov
 * @version 1.0
 */
public class NoFindMusicException extends Exception{

    /**
     * Создает исключение NoFindMusicException с указанным
     * подробным сообщением
     */
    public NoFindMusicException(String reason) {
        super(reason);
    }

    /**
     * Создает исключение NoFindMusicException с указанным
     * базового сообщения
     */
    public NoFindMusicException() {
        super("Музыка не была найдена в источнике");
    }
}
